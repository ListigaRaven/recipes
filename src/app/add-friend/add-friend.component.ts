import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-friend',
  templateUrl: './add-friend.component.html',
  styleUrls: ['./add-friend.component.less']
})
export class AddFriendComponent {

  friend = '';

  constructor(private dialogRef: MatDialogRef<AddFriendComponent>) {}

  cancel() {
    this.dialogRef.close();
  }

  add() {
    this.dialogRef.close(this.friend);
  }
}
