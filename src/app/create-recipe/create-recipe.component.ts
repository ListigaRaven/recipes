import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-create-recipe',
  templateUrl: './create-recipe.component.html',
  styleUrls: ['./create-recipe.component.less']
})
export class CreateRecipeComponent {

  constructor(private dialogRef: MatDialogRef<CreateRecipeComponent>) {}

  create() {

  }

  cancel() {
    this.dialogRef.close();
  }
}
