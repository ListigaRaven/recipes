import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { FlexModule } from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatIconModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatButtonModule } from '@angular/material';
import { AngularFireModule } from '@angular/fire';

import { AppComponent } from './app.component';
import { CreateRecipeComponent } from './create-recipe';
import { ViewRecipeComponent } from './view-recipe';
import { AddFriendComponent } from './add-friend';

export const config = {
  apiKey: "AIzaSyB1IZVo2dZU5Kk4IzWdek_ClvymgpsRYfQ",
  authDomain: "recipes-14c5a.firebaseapp.com",
  databaseURL: "https://recipes-14c5a.firebaseio.com",
  projectId: "recipes-14c5a",
  storageBucket: "recipes-14c5a.appspot.com",
  messagingSenderId: "209738140072",
};

const dialogComponents = [
  CreateRecipeComponent,
  ViewRecipeComponent,
  AddFriendComponent,
]

const components = [
  AppComponent,
  ...dialogComponents,
]

const modules = [
  BrowserModule,
  BrowserAnimationsModule,
  FormsModule,
  FlexModule,
  MatIconModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
	AngularFireModule.initializeApp(config, 'recipes'),
]

@NgModule({
  declarations: [components],
  imports: [modules],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [dialogComponents],
})
export class AppModule { }
