import { Component } from '@angular/core';
import { MatDialog, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material';
import { CreateRecipeComponent } from './create-recipe';
import { ViewRecipeComponent } from './view-recipe';
import { AddFriendComponent } from './add-friend';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {

  myRecipes = [ [{title: 'Steak', ingredients: 'meat\nrosemary\nonion\ngarlic', directions: 'cook in cast iron'}, {title: 'Steak', ingredients: 'meat\nrosemary\nonion\ngarlic', directions: 'cook in cast iron'}], [{title: 'Steak', ingredients: 'meat\nrosemary\nonion\ngarlic', directions: 'cook in cast iron'}]]

  friends = [{id: 1, name: 'Jonny'}, {id: 2, name: 'Baylan'}, {id: 3, name: 'Bridger'}, {id: 4, name: 'Jared'}, {id: 5, name: 'Alex'}, {id: 6, name: 'Austen'}, {id: 7, name: 'Wini'}];
  selectedPerson = 0;
  showDropDown = false;
  recipeType = 'uploads';

  constructor(private dialog: MatDialog) {}

  selectPerson(id) {
    this.selectedPerson = id;
  }

  createRecipe() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.height = '60vh';
    dialogConfig.width = '30vw';
    dialogConfig.disableClose = true;

    this.dialog.open(CreateRecipeComponent, dialogConfig);
  }

  viewRecipe() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.height = '60vh';
    dialogConfig.width = '30vw';
    dialogConfig.disableClose = true;

    dialogConfig.data = {
      title: 'Steak',
      ingredients: 'meat\nrosemary\nonion\ngarlic',
      directions: 'cook in cast iron',
    }

    this.dialog.open(ViewRecipeComponent, dialogConfig);
  }

  addFriend() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.height = '30vh';
    dialogConfig.width = '30vw';
    dialogConfig.disableClose = true;

    this.dialog.open(AddFriendComponent, dialogConfig).afterClosed().subscribe((friend) => {
      if(friend) {
        this.friends.push({id: (this.friends[this.friends.length - 1].id + 1), name: friend});
      }
    });
  }
}
