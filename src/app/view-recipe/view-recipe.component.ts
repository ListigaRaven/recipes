import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-view-recipe',
  templateUrl: './view-recipe.component.html',
  styleUrls: ['./view-recipe.component.less']
})
export class ViewRecipeComponent {

  recipe: any = {}

  constructor(private dialogRef: MatDialogRef<ViewRecipeComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.recipe = data;
  }

  create() {

  }

  cancel() {
    this.dialogRef.close();
  }
}
